# INVENTORY

Provide in inventory interface. The easiest way to use this is with the api.Model classes. These can be generated with the following

```python
from inventory import load_models


async def func():
    models = load_models()
    model = models[0]
    result = model.create(...)
    return result
```