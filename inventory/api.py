from dataclasses import dataclass
from typing import Optional, Type, TypeVar

from sqlalchemy import select
from sqlalchemy.sql import Delete, Select
from sqlmodel import SQLModel

from . import database
from .models import _base
from .exceptions import NoEngine

ST = TypeVar("ST", Select, Delete)


@dataclass
class Model:
    model: Type[_base.Table]
    _engine: Optional[database.Engine] = None

    def __post_init__(self):
        if Model._engine is None:
            message = f"No engine set for class Model, using model {self.model.__name__}"
            raise NoEngine(message)

    @property
    def schema(self) -> dict[str, Type]:
        data = {}
        for name, field in self.model.__fields__.items():
            data[name] = field.type_
        return data

    def create(self, **kwargs) -> _base.Table:
        instance = self.model(**kwargs)
        with database.connection(Model._engine) as db:  # type: ignore
            db.add(instance)
            db.commit()
            db.refresh(instance)
            return instance

    def get(self, id_: int) -> Optional[_base.Table]:
        statement = select(self.model).where(self.model.id == id_)
        with database.connection(Model._engine) as db:  # type: ignore
            results = db.execute(statement)
            items = results.scalars().all()
            if len(items) == 0:
                return None
            return items[0]

    def search(
        self, limit: Optional[int] = None, offset: Optional[int] = None, **params
    ) -> list[_base.Table]:
        init = select(self.model)
        statement = self._search_statement(init, params)
        statement = statement.order_by(self.model.id)
        if limit is not None:
            statement = statement.limit(limit)
        if offset is not None:
            statement = statement.offset(offset)
        with database.connection(Model._engine) as db:  # type: ignore
            results = db.execute(statement)
            return results.scalars().all()

    def update(self, id_: int, **kwargs) -> Optional[_base.Table]:
        instance = self.get(id_)
        for key, value in kwargs.items():
            if hasattr(instance, key):
                setattr(instance, key, value)
        with database.connection(Model._engine) as db:  # type: ignore
            db.commit()
            db.refresh(instance)
            return instance

    def delete(self, id_: int) -> bool:
        instance = self.get(id_)
        with database.connection(Model._engine) as db:  # type: ignore
            db.delete(instance)
            db.commit()
        return self.get(id_) is None

    def _search_statement(
        self,
        statement: ST,
        params: dict,
    ) -> ST:
        retval = statement
        for key, value in params.items():
            if value is not None:
                attr = getattr(self.model, key)
                retval = retval.where(attr == key)
        return retval


def create_database(connection_string: str):
    engine = database.connect(connection_string)
    SQLModel.metadata.create_all(engine)
    Model._engine = engine
