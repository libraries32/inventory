from types import ModuleType
from typing import Iterable, Iterator, Type

from . import api, models
from .models._base import Table

__all__ = ["load_models"]


def load_models() -> Iterable[api.Model]:
    for model in find_all_models():
        yield api.Model(model)


def find_packages() -> Iterator[ModuleType]:
    for pkg in dir(models):
        if not pkg.startswith("_"):
            yield getattr(models, pkg)


def find_all_models() -> Iterator[Type[Table]]:
    for package in find_packages():
        for model in find_data_models(package):
            yield model


def find_data_models(package: ModuleType) -> Iterator[Type[Table]]:
    trimmed = (
        getattr(package, model) for model in dir(package) if not model.startswith("_")
    )
    return filter(lambda m: hasattr(m, "__table__"), trimmed)
