from contextlib import contextmanager
from sqlite3 import IntegrityError

from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session


def connect(string: str) -> Engine:
    return create_engine(string)


@contextmanager
def connection(engine: Engine):
    with Session(engine) as session:
        try:
            yield session
        except IntegrityError:
            session.rollback()
